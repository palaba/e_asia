<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get user pages.
     */
    public function pages()
    {
        return $this->hasMany('App\Page');
    }

    /**
     * Get all of the tags for the user.
     */
    public function tags()
    {
        // return $this->hasManyThrough('App\Tag', 'App\Page');

        return Tag::join('tag_page', 'tag.id', '=', 'tag_page.tag_id')
        ->join('pages', 'tag_page.page_id', '=', 'pages.id')
        ->join('users', 'page.user_id', '=', 'user.id')
        ->where('user.id', $this->id);
    }

    public function getTagsAttribute()
    {
        if (!$this->relationLoaded('pages') || !$this->pages->first()->relationLoaded('tags')) {
            $this->load('pages.tags');
        }

        return collect($this->pages->pluck('tags'))->collapse()->unique();
    }
}

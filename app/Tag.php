<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The pages that belong to the tag.
     */
    public function pages()
    {
        return $this->belongsToMany('App\Page');
    }
}

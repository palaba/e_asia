<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SmsRepositoryInterface;

class SmsController extends Controller
{
	public function __construct(SmsRepositoryInterface $sms)
	{
		$this->sms = $sms;
	}

	public function create() {
		return view('sms');
	}

	public function sendSMS(Request $request) {
		$phone_number = $request->input('phone_number');
		$message = $request->input('message');

		$response = $this->sms->sendMessage($phone_number, $message);

		return view('sms', compact('response'));
	}
}

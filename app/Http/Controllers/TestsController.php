<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestsController extends Controller
{
    public function populateDB() {
		\Artisan::call('migrate:refresh', [
	        '--seed' => 'default'
	    ]);
		
	    return redirect('/');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * The user that owns the page.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * The tags that belong to the page.
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tag');
    }
}

<?php

namespace App\Repositories;

use Illuminate\Support\ServiceProvider;

class SmsBackendServiceProvider extends ServiceProvider
{
    public function register()
    {
    	$this->app->bind('App\Repositories\SmsRepositoryInterface', 'App\Repositories\SmsRepository');
    }
}

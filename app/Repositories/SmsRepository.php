<?php

namespace App\Repositories;

class SmsRepository implements SmsRepositoryInterface
{
    const REQUEST_URL = 'https://textbelt.com/text';

	private $api_key;

	function __construct()
	{
		$this->api_key = config('services.textbelt.api_key');
	}

    public function sendMessage($phone_number, $message)
    {
    	if(empty($message)) {
    		throw new \Exception('Empty message');
    	}

    	$ch = curl_init(self::REQUEST_URL);
    	$data = array(
    	  'phone' => $phone_number,
    	  'message' => $message,
    	  'key' => $this->api_key,
    	);

    	curl_setopt($ch, CURLOPT_POST, 1);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    	$response = curl_exec($ch);
    	curl_close($ch);

    	return json_decode($response);
    }
}

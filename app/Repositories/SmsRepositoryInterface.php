<?php

namespace App\Repositories;

interface SmsRepositoryInterface
{
    public function sendMessage($phone_number, $message);
}

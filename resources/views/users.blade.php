<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>REKRUTACJA E FIRST ASIA</title>

        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- Materialize CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <!-- Styles -->
        <style>
            html {
                font-size: 10px;
            }

            body {
                padding: .75rem 0;
                font-size: 1.4rem;
            }

            .btn {
                font-size: 1.4rem;
            }

            .btn > i {
                font-size: 2rem;
            }

            .card .card-title {
                font-size: 2.4rem;
                font-weight: 300;
            }

            .card .card-content p,
            .card .card-reveal p {
                font-size: 1.3rem;
                margin-bottom: .5rem;
            }

            .card .card-reveal p > a {
                color: #ffab40;
            }

            .card .card-action a,
            .card .card-action a > i {
                font-size: 1.3rem;
            }

            .chip {
                font-size: 1.2rem;
            }

            .btn-large i {
                font-size: 2rem;
            }

            hr {
                border-width: 0;
                height: 1px;
                background-color: #e4e4e4;
            }

            .input-field {
                margin-top: 1.5rem;
                margin-bottom: 1.5rem;
            }

            .input-field > label {
                font-size: 1.3rem;
                line-height: 1.3rem;
            }

            pre {
                font-size: 1.4rem;
                white-space: pre-line;
                padding: 1rem;
                border: 1px solid #e4e4e4;
                border-radius: .6rem;
            }

            @media(max-width: 1400px) {
                html {
                    font-size: 9px;
                }
            }
        </style>
    </head>
    <body class="grey lighten-4">
        <div class="row">
            @forelse($users as $user)
            <div class="col s12 m6 l4 xl3">
                <div class="card">
                    <div class="card-image">
                        <img src="https://images.pexels.com/photos/1250260/pexels-photo-1250260.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500">
                        <span class="card-title">{{ $user->name }}</span>
                        <a class="activator btn-large btn-floating halfway-fab waves-effect waves-light green"><i class="material-icons">filter_none</i></a>
                    </div>
                    <div class="card-content">
                        <p>
                            ID: <strong>{{ $user->id }}</strong>
                        </p>
                        <p>
                            E-mail:
                            <br>
                            <strong>{{ $user->email }}</strong>
                        </p>
                        <p>
                            Tagi:
                            <br>
                            <div class="chips-holder">
                                @forelse($user->tags as $tag)
                                <div class="chip green lighten-5 green-text text-darken-4">{{ $tag->title }}</div>
                                @empty
                                Brak tagów użytkownika
                                @endforelse
                            </div>
                        </p>
                    </div>
                    <div class="card-reveal">
                        <span class="card-title grey-text text-darken-4">Strony:<i class="material-icons right">close</i></span>
                        @forelse($user->pages as $key => $page)
                            @if($key != 0)
                            <hr>
                            @endif
                        <p>
                            Nazwa:
                            <br>
                            <strong>{{ $page->title }}</strong>
                        </p>
                        <p>
                            URL:
                            <br>
                            <a href="{{ $page->url }}" target="_blank"><em>{{ $page->url }}</em></a>
                        </p>
                        <p>
                            Tagi strony:
                            <br>
                            @forelse($page->tags as $tag)
                            <div class="chip green lighten-5 green-text text-darken-4">{{ $tag->title }}</div>
                            @empty
                            Brak tagów dla strony
                            @endforelse
                        </p>
                        @empty
                        <p>Brak stron użytkownika</p>
                        @endforelse
                    </div>
                    <div class="card-action">
                        <a href="mailto:{{ $user->email }}"><i class="material-icons">email</i> Napisz wiadomość</a>
                    </div>
                </div>
            </div>
            @empty
            <p class="center-align">Brak użytkowników</p>
            @endforelse
        </div>
        <div class="row">
            <div class="center-align">
                <a href="{{ url('/generate') }}" class="btn waves-effect waves-light green">Generuj dane testowe<i class="material-icons right">autorenew</i></a>
            </div>
        </div>
        <div class="row">
            <div class="center-align">
                <a href="{{ url('/send-sms') }}" class="btn waves-effect waves-light green lighten-5 green-text text-darken-4">Test SMS API<i class="material-icons right">message</i></a>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                equalHeights('.chips-holder');
            });

            window.addEventListener('resize', function(){
               equalHeights('.chips-holder');
            });

            function equalHeights(selector) {
                var elements = document.querySelectorAll(selector);

                if(window.innerWidth > 600) {
                    var max_height = 0;

                    Array.prototype.forEach.call(elements, function(el, i){
                        el.style.height = 'auto';

                        if(el.offsetHeight > max_height) {
                            max_height = el.offsetHeight;
                        }
                    });

                    Array.prototype.forEach.call(elements, function(el, i){
                        el.style.height = max_height + 'px';
                    });
                } else {
                    Array.prototype.forEach.call(elements, function(el, i){
                        el.style.height = 'auto';
                    });
                }
            }
        </script>
    </body>
</html>

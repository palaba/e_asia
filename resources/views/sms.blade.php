<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>REKRUTACJA E FIRST ASIA</title>

        <!--Import Google Icon Font-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- Materialize CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        <!-- Styles -->
        <style>
            html {
                font-size: 10px;
            }

            body {
                padding: .75rem 0;
                font-size: 1.4rem;
            }

            .btn {
                font-size: 1.4rem;
            }

            .btn > i {
                font-size: 2rem;
            }

            .card .card-title {
                font-size: 2.4rem;
                font-weight: 300;
            }

            .card .card-content p,
            .card .card-reveal p {
                font-size: 1.3rem;
                margin-bottom: .5rem;
            }

            .card .card-reveal p > a {
                color: #ffab40;
            }

            .card .card-action a,
            .card .card-action a > i {
                font-size: 1.3rem;
            }

            .chip {
                font-size: 1.2rem;
            }

            .btn-large i {
                font-size: 2rem;
            }

            hr {
                border-width: 0;
                height: 1px;
                background-color: #e4e4e4;
            }

            .input-field {
                margin-top: 1.5rem;
                margin-bottom: 1.5rem;
            }

            .input-field > label {
                font-size: 1.3rem;
                line-height: 1.3rem;
            }

            pre {
                font-size: 1.4rem;
                white-space: pre-line;
                padding: 1rem;
                border-radius: .6rem;
            }

            @media(max-width: 1400px) {
                html {
                    font-size: 9px;
                }
            }
        </style>
    </head>
    <body class="grey lighten-4">
        @if(config('services.textbelt.api_key') == '')
        <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4">
                <div class="card-panel red lighten-5 red-text text-darken-4">
                    Zmienna środowiskowa <strong>TEXTBELT_KEY</strong> nie została zdefiniowana w pliku <em><strong>.env</strong></em>. Aplikacja nie będzie działać poprawnie.
                </div>
            </div>
        </div>
        @endif
        @if(isset($response) && $response->success)
        <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4">
                <div class="card-panel green lighten-5 green-text text-darken-4">
                    Wiadomość SMS (<strong>{{ $response->textId }}</strong>) została pomyślnie wysłana.
                    <br>
                    Można jeszcze wysłać <strong>{{ $response->quotaRemaining }}</strong> wiadomości.
                </div>
            </div>
        </div>
        @elseif(isset($response) && !$response->success)
        <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4">
                <div class="card-panel red lighten-5 red-text text-darken-4">
                    Wystąpił błąd podczas wysyłania wiadomości. Treść błędu:
                    <br>
                    <em><strong>{{ $response->error }}</strong></em>
                </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4">
                <div class="card">
                    <form action="" method="POST" class="card-content">
                        <div class="row">
                            <span class="card-title">Wyślij SMS</span>
                        </div>
                        @csrf
                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">phone</i>
                                <input type="tel" pattern="^[0-9+]*$" id="phone_number" class="materialize-textarea" name="phone_number" required>
                                <label for="phone_number">Numer telefonu</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field">
                                <i class="material-icons prefix">message</i>
                                <textarea id="message" class="materialize-textarea" name="message" maxlength="160" required></textarea>
                                <label for="message">Treść wiadomości</label>
                            </div>
                        </div>
                        <button class="btn waves-effect waves-light green" type="submit" name="action">
                            Wyślij <i class="material-icons right">send</i>
                        </button>
                    </form>
                </div>
            </div>
        </div>
        @if(isset($response))
        <div class="row">
            <div class="col s12 m6 offset-m6 l4 offset-l4">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <span class="card-title">Odpowiedź z API</span>
                        </div>
                        <pre class="grey lighten-4">
                            {{ var_dump($response) }}
                        </pre>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="center-align">
                <a href="{{ url('/') }}" class="btn waves-effect waves-light green lighten-5 green-text text-darken-">Test database seeder<i class="material-icons right">autorenew</i></a>
            </div>
        </div>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    </body>
</html>

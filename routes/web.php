<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

Route::get('/', 'UsersController@index');

Route::get('/generate', 'TestsController@populateDB');

Route::get('/send-sms', 'SmsController@create');

Route::post('/send-sms', 'SmsController@sendSMS');

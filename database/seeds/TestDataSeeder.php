<?php

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 10)->create()->each(function ($user) {
        	$user->pages()->saveMany(factory(App\Page::class, rand(1, 5))->make());
        });

        App\Page::all()->each(function ($page) {
        	$page->tags()->saveMany(factory(App\Tag::class, rand(1, 3))->make());
        });
    }
}

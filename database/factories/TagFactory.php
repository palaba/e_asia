<?php

use Faker\Generator as Faker;

$factory->define(App\Tag::class, function (Faker $faker) {
    return [
        'title' => ucfirst($faker->words(2, true)),
    ];
});
